#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/imgproc/imgproc.hpp>

cv::Mat src, src_gray, dst, detected_edges;
int edgeThreshold = 1, threshold = 50, ratio = 3, kernel_size = 3;
image_transport::Publisher pub;

void callback(const sensor_msgs::ImageConstPtr& msg){
    src = cv_bridge::toCvShare(msg,"bgr8")->image;
    cv::cvtColor(src, src_gray, CV_BGR2GRAY);
    cv::blur(src_gray, detected_edges, cv::Size(3,3));
    cv::Canny(detected_edges, detected_edges, threshold, threshold*ratio, kernel_size);
    dst.create(src.size(), src.type());
    dst = cv::Scalar::all(0);
    src.copyTo(dst, detected_edges);
    sensor_msgs::ImagePtr msgPub = cv_bridge::CvImage(std_msgs::Header(), "bgr8", dst).toImageMsg();
    pub.publish(msgPub);
}

int main(int argc, char **argv){
    ros::init(argc, argv, "canny");
    ros::NodeHandle n;
    image_transport::ImageTransport it(n);
    pub = it.advertise("canny_image", 1);
    image_transport::Subscriber sub = it.subscribe("/cv_camera/image_raw", 1, callback);
    ros::spin();
}

